'use strict'

const fs = require('fs')
const path = require('path')
const download = require('./Downloader')
const ShortBus = require('shortbus')
const GitHubApi = require("github")
const EventEmitter = require('events').EventEmitter

/**
 * @class CDN
 * Represents the CDN Github repo
 */
class CDN extends EventEmitter {
  constructor (repo, path, token) {
    super()

    Object.defineProperties(this, {
      account: {
        enumerable: true,
        writable: false,
        configurable: false,
        value: repo.split('/')[0]
      },
      repo: {
        enumerable: true,
        writable: false,
        configurable: false,
        value: repo.split('/')[1]
      },
      fullrepo: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: repo
      },
      path: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: path
      },
      token: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: token
      },
      headers: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: {
          'user-agent': repo.split('/')[0] + ' ' + repo.split('/')[1] + ' release manager',
          'Authorization': 'token ' + token
        }
      },
      filecount: {
        enumerable: false,
        writable: true,
        configurable: false,
        value: 0
      },
      assets: {
        enumerable: false,
        writable: true,
        configurable: false,
        value: []
      },
      attempts: {
        enumerable: false,
        writable: true,
        configurable: false,
        value: 0
      },
      github: {
        enumerable: false,
        writable: false,
        configurable: false,
        value: new GitHubApi({
          // optional
          debug: false,
          protocol: 'https',
          host: 'api.github.com', // should be api.github.com for GitHub
          // pathPrefix: "/api/v3", // for some GHEs; none for GitHub
          timeout: 5000,
          headers: {
            'user-agent': repo.split('/')[0] + ' ' + repo.split('/')[1] + ' release manager',
            'Authorization': 'token ' + token
          },
          followRedirects: false, // default: true; there's currently an issue with non-get redirects, so allow ability to disable follow-redirects
        })
      }
    })
  }

  get repository () {
    return this.fullrepo
  }

  notify (msg) {
    this.emit('status', msg)
  }

  authenticate () {
    this.github.authenticate({
      type: 'oauth',
      token: this.token
    })
  }

  /**
   * @method fileList
   * Retrieve the file list from the CDN repo.
   * @param  {Function} callback
   * @param {array} callback.assets
   * A list of assets. Each array item is an object:
   * ```
   * {
   * 	 namw: <name>,
   *   path: <filepath>,
   *   sha: <SHASUM>
   * }
   * ```
   */
  fileList (callback) {
    this.authenticate()
    this.github.repos.getContent({
      user: this.account,
      repo: this.repo,
      path: this.path
    }, function(err, res) {
      if (err) {
        throw err
      }

      const assets = res.filter(function (file) {
        return file.name.trim().toLowerCase().indexOf('readme') < 0
          && (path.extname(file.path) === '.js' || path.extname(file.path) === '.min.js')
      }).map(function (file) {
        return {
          name: file.name,
          path: file.path,
          sha: file.sha
        }
      })

      callback(assets)
    })
  }

  removeFile (filepath, sha, message, callback) {
    const me = this
    this.github.repos.deleteFile({
      user: this.account,
      repo: this.repo,
      path: filepath,
      message: message,
      sha: sha
    }, function (err) {
      if (err) {
        if (err.message) {
          if (err.message.toLowerCase().indexOf('not found') >= 0) {
            console.log(filepath + 'not found in ' + me.fullrepo)
            return callback()
          }
        }
        throw err
      } else {
        callback()
      }
    })
  }

  /**
   * @method clearReleaseFiles
   * Remove all files from the repo.
   * @param  {Function} callback
   * Executed upon completion.
   */
  clearReleaseFiles (callback) {
    const tasks = new ShortBus()
    const me = this

    this.notify('Clearing ' + this.fullrepo + '/' + this.path + '...')

    this.fileList(function (assets) {
      if (assets.length === 0) {
        return callback()
      }

      assets.forEach(function (asset) {
        me.notify('Queuing removal of ' + asset.path)
        tasks.add('Remove ' + asset.path, function (next) {
          const task = this
          me.removeFile(asset.path, asset.sha, 'Replacing or removing ' + asset.name, function () {
            me.notify(task.number + '\) ' + task.name)
            setTimeout(next, 1500)
          })
        })
      })

      tasks.on('complete', function () {
        // Check to make sure all the files were removed
        me.fileList(function (assets) {
          if (assets.length > 0) {
            if (me.attempts < 4) {
              me.notify(assets.length + ' files still remain. Attempting to remove (again).')
              me.attempts++
              me.clear(callback)
            } else {
              me.notify('Could not remove all files after 3 attempts. Aborting.')
              throw new Error('Could not remove all files after 3 attempts. Aborting.')
            }
          } else {
            me.notify('All files removed.')
            me.attempts = 0
            callback()
          }
        })
      })

      tasks.process(true)
    })
  }

  clearHostedFiles (remotepath, callback) {
    const me = this

    this.authenticate()

    download('https://api.github.com/repos/' + this.fullrepo + '/contents/' + remotepath + '?ref=gh-pages', this.headers, function (res) {
      if (res.message) {
        return callback()
      }
      if (res.length === 0) {
        return callback()
      }

      const assets = res.map(function (file) {
        return {
          name: file.name,
          path: file.path,
          sha: file.sha
        }
      })

      const tasks = new ShortBus()

      assets.forEach(function (file) {
        tasks.add('Removing ' + file.name + ' from ' + file.path, function (next) {
          const task = this
          me.github.repos.deleteFile({
            user: me.account,
            repo: me.repo,
            path: file.path,
            sha: file.sha,
            branch: 'gh-pages',
            message: 'Clearing ' + file.path + '.'
          }, function (err) {
            if (err) {
              throw err
            }
            me.notify(task.number + '\) ' + task.name)
            setTimeout(next, 2000)
          })
        })
      })

      tasks.on('complete', callback)
      tasks.process(true)
    })
  }

  /**
   * @method publishGithubPagesFiles
   * Upload files to the CDN gh-pages site.
   * @param {array} filepaths
   * @param {function} callback
   */
  publishGithubPagesFiles (files, pathprefix, callback) {
    const me = this
    const tasks = new ShortBus()

    if (typeof pathprefix === 'function') {
      callback = pathprefix
      pathprefix = me.path
    }
    pathprefix = pathprefix || me.path

    // Clear the existing content
    tasks.add('Clear hosted files if necessary.', function (next) {
      me.clearHostedFiles(pathprefix, next)
    })

    files.forEach(function (file) {
      tasks.add('Published ' + path.basename(file) + ' to ' + path.join(pathprefix), function (next) {
        const task = this
        fs.readFile(file, {
          encoding: 'base64'
        }, function (err, buffer) {
          if (err) {
            me.notify('ERROR publishing ' + file + ': ' + (err.message || 'Unknown'))
            return next()
          }

          me.github.repos.createFile({
            user: me.account,
            repo: me.repo,
            path: path.join(pathprefix, path.basename(file)),
            branch: 'gh-pages',
            content: buffer,
            message: 'Adding ' + path.basename(file) + ' to hosted files.'
          }, function (err) {
            if (err) {
              console.log('ERROR publishing ' + path.basename(file) + ': ' + (err.message || 'Unknown'))
              throw err
            }
            me.notify(task.number + '\) ' + task.name)
            setTimeout(next, 2000)
          })
        })
      })
    })

    tasks.on('complete', callback)

    // Delay start by 10 seconds so Github completes SHA creation
    // on prior processes.
    setTimeout(function () {
      tasks.process(true)
    }, 10000)
  }

  /**
   * @method uploadReleaseFiles
   * Upload files to the CDN directory.
   * @param {array} assets
   * An array of local filepaths to upload.
   * @param {function} callback
   */
  uploadReleaseFiles (assets, callback) {
    this.notify('Uploading release assets:\n\t\t- ' + assets.join('\n\t\t- '))
    if (assets.length === 0) {
      return callback()
    }

    const me = this
    const tasks = new ShortBus()

    assets.forEach(function (asset) {
      me.notify('Queued upload of ' + asset)
      tasks.add('Uploaded ' + asset, function (next) {
        const task = this
        me.commitReleaseFile(asset, function () {
          me.notify(task.number + '\) ' + task.name)
          next()
        })
      })
    })

    tasks.on('complete', callback)
    tasks.process(true)

    // this.uploadFiles(dir, uploads, function () {
    //   setTimeout(function () {
    //     me.fileList(function (assets) {
    //       if (assets.length !== uploads.length) {
    //         if (me.attempts < 4) {
    //           me.notify(assets.length + ' file' + (assets.length === 1 ? '' : 's') + ' uploaded. Expected ' + uploads.length + '. Attempting to upload missing files (again)')
    //           me.attempts++
    //
    //           let remaining = uploads.filter(function (file) {
    //             return assets.indexOf(file) < 0
    //           })
    //
    //           me.uploadFiles(dir, remaining, callback)
    //         } else {
    //           me.notify('Could not upload all files, even after 3 attempts. Aborting.')
    //           throw new Error('Could not upload all files, even after 3 attempts. Aborting.')
    //         }
    //       } else {
    //         me.notify('All files uploaded.')
    //         me.attempts = 0
    //         callback()
    //       }
    //     })
    //   }, 3000)
    // })
  }

  commitReleaseFile (filepath, callback) {
    const me = this
    fs.readFile(filepath, {
      encoding: 'base64'
    }, function (err, buffer) {
      if (err) {
        throw err
      }

      me.github.repos.createFile({
        user: me.account,
        repo: me.repo,
        path: me.path + '/' + path.basename(filepath),
        content: buffer,
        message: 'Added ' + path.basename(filepath) + ' to CDN release.'
      }, function (err) {
        if (err) {
          me.notify('ERROR pushing ' + path.basename(filepath) + ': ' + (err.message || 'Unknown'))
        }
        setTimeout(callback, 3000)
      })
    })
  }

  uploadFiles (dir, list, callback) {
    if (list.length === 0) {
      return callback()
    }

    const me = this
    const tasks = new ShortBus()

    list.forEach(function (file) {
      me.notify('Queuing upload of ' + file)
      tasks.add('Uploaded ' + file, function (next) {
        const task = this
        fs.readFile(path.join(dir, file), {
          encoding: 'base64'
        }, function (err, buffer) {
          me.github.repos.createFile({
            user: me.account,
            repo: me.repo,
            path: me.path + '/' + file,
            content: buffer,
            message: 'Added ' + file + ' from release.'
          }, function (err) {
            if (err) {
              me.notify('ERROR ' + task.number + '\) ' + task.name + ': ' + (err.message || 'Unknown'))
            } else {
              me.notify(task.number + '\) ' + task.name)
            }
            setTimeout(next, 3000)
          })
        })
      })
    })

    tasks.on('complete', function () {
      callback()
    })

    tasks.process(true)
  }
}

module.exports = CDN
