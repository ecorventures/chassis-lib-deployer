'use strict'

const https = require('https')

/**
 * @method download
 * @param  {string} uri
 * The URL to download from.
 * @param  {object} [headers={}]
 * Custom HTTP request headers.
 * @param  {Function} callback
 * Callback executed when the download is complete.
 * @param {String|Object} callback.content
 * The content of the download.
 */
const download = function (uri, headers, callback) {
  uri = require('url').parse(uri)
  let to

  if (typeof headers === 'function') {
    callback = headers
    headers = {}
  }

  const req = https.request({
    method: 'GET',
    port: 443,
    hostname: uri.hostname,
    path: uri.path,
    headers: headers
  }, function (res) {

    res.setEncoding('utf8')

    let body = ''
    res.on('data', function (chunk) {
      body += chunk
    })

    res.on('end', function () {
      clearTimeout(to)
      if (res.statusCode === 403) {
        throw new Error(body.message)
        return
      }

      if (res.statusCode === 301 || res.statusCode === 302) {
        if (!res.headers.location) {
          throw new Error('Attempted to follow a redirect for ' + uri.href + ' with no redirect location specified by the remote server.')
        }
        download(res.headers.location, {}, callback)
        return
      }

      try {
        body = JSON.parse(body)
      } catch (e) {
        body = body.trim()
      }
      callback(body)
    })
  })

  req.on('error', function (err) {
    clearTimeout(to)
    throw err
  })

  req.end()
  to = setTimeout(function () {
    throw new Error('Timeout retrieving ' + uri)
  }, 30000)
}

module.exports = download
